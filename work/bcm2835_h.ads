pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with aarch64_linux_gnu_sys_types_h;
with stddef_h;
with aarch64_linux_gnu_bits_stdint_uintn_h;
with Interfaces.C.Strings;

package bcm2835_h is

   BCM2835_VERSION : constant := 10066;  --  ../bcm2835.h:608

   HIGH : constant := 16#1#;  --  ../bcm2835.h:630

   LOW : constant := 16#0#;  --  ../bcm2835.h:632
   --  arg-macro: function MIN (a, b)
   --    return a < b ? a : b;

   BCM2835_CORE_CLK_HZ : constant := 250000000;  --  ../bcm2835.h:640

   BMC2835_RPI2_DT_FILENAME : aliased constant String := "/proc/device-tree/soc/ranges" & ASCII.NUL;  --  ../bcm2835.h:643

   BCM2835_PERI_BASE : constant := 16#20000000#;  --  ../bcm2835.h:652

   BCM2835_PERI_SIZE : constant := 16#01000000#;  --  ../bcm2835.h:654

   BCM2835_RPI2_PERI_BASE : constant := 16#3F000000#;  --  ../bcm2835.h:656

   BCM2835_RPI4_PERI_BASE : constant := 16#FE000000#;  --  ../bcm2835.h:658

   BCM2835_RPI4_PERI_SIZE : constant := 16#01800000#;  --  ../bcm2835.h:660

   BCM2835_ST_BASE : constant := 16#3000#;  --  ../bcm2835.h:665

   BCM2835_GPIO_PADS : constant := 16#100000#;  --  ../bcm2835.h:667

   BCM2835_CLOCK_BASE : constant := 16#101000#;  --  ../bcm2835.h:669

   BCM2835_GPIO_BASE : constant := 16#200000#;  --  ../bcm2835.h:671

   BCM2835_SPI0_BASE : constant := 16#204000#;  --  ../bcm2835.h:673

   BCM2835_BSC0_BASE : constant := 16#205000#;  --  ../bcm2835.h:675

   BCM2835_GPIO_PWM : constant := 16#20C000#;  --  ../bcm2835.h:677

   BCM2835_AUX_BASE : constant := 16#215000#;  --  ../bcm2835.h:679

   BCM2835_SPI1_BASE : constant := 16#215080#;  --  ../bcm2835.h:681

   BCM2835_SPI2_BASE : constant := 16#2150C0#;  --  ../bcm2835.h:683

   BCM2835_BSC1_BASE : constant := 16#804000#;  --  ../bcm2835.h:685

   BCM2835_PAGE_SIZE : constant := (4*1024);  --  ../bcm2835.h:768

   BCM2835_BLOCK_SIZE : constant := (4*1024);  --  ../bcm2835.h:770

   BCM2835_GPFSEL0 : constant := 16#0000#;  --  ../bcm2835.h:780
   BCM2835_GPFSEL1 : constant := 16#0004#;  --  ../bcm2835.h:781
   BCM2835_GPFSEL2 : constant := 16#0008#;  --  ../bcm2835.h:782
   BCM2835_GPFSEL3 : constant := 16#000c#;  --  ../bcm2835.h:783
   BCM2835_GPFSEL4 : constant := 16#0010#;  --  ../bcm2835.h:784
   BCM2835_GPFSEL5 : constant := 16#0014#;  --  ../bcm2835.h:785
   BCM2835_GPSET0 : constant := 16#001c#;  --  ../bcm2835.h:786
   BCM2835_GPSET1 : constant := 16#0020#;  --  ../bcm2835.h:787
   BCM2835_GPCLR0 : constant := 16#0028#;  --  ../bcm2835.h:788
   BCM2835_GPCLR1 : constant := 16#002c#;  --  ../bcm2835.h:789
   BCM2835_GPLEV0 : constant := 16#0034#;  --  ../bcm2835.h:790
   BCM2835_GPLEV1 : constant := 16#0038#;  --  ../bcm2835.h:791
   BCM2835_GPEDS0 : constant := 16#0040#;  --  ../bcm2835.h:792
   BCM2835_GPEDS1 : constant := 16#0044#;  --  ../bcm2835.h:793
   BCM2835_GPREN0 : constant := 16#004c#;  --  ../bcm2835.h:794
   BCM2835_GPREN1 : constant := 16#0050#;  --  ../bcm2835.h:795
   BCM2835_GPFEN0 : constant := 16#0058#;  --  ../bcm2835.h:796
   BCM2835_GPFEN1 : constant := 16#005c#;  --  ../bcm2835.h:797
   BCM2835_GPHEN0 : constant := 16#0064#;  --  ../bcm2835.h:798
   BCM2835_GPHEN1 : constant := 16#0068#;  --  ../bcm2835.h:799
   BCM2835_GPLEN0 : constant := 16#0070#;  --  ../bcm2835.h:800
   BCM2835_GPLEN1 : constant := 16#0074#;  --  ../bcm2835.h:801
   BCM2835_GPAREN0 : constant := 16#007c#;  --  ../bcm2835.h:802
   BCM2835_GPAREN1 : constant := 16#0080#;  --  ../bcm2835.h:803
   BCM2835_GPAFEN0 : constant := 16#0088#;  --  ../bcm2835.h:804
   BCM2835_GPAFEN1 : constant := 16#008c#;  --  ../bcm2835.h:805
   BCM2835_GPPUD : constant := 16#0094#;  --  ../bcm2835.h:806
   BCM2835_GPPUDCLK0 : constant := 16#0098#;  --  ../bcm2835.h:807
   BCM2835_GPPUDCLK1 : constant := 16#009c#;  --  ../bcm2835.h:808

   BCM2835_GPPUPPDN0 : constant := 16#00e4#;  --  ../bcm2835.h:811
   BCM2835_GPPUPPDN1 : constant := 16#00e8#;  --  ../bcm2835.h:812
   BCM2835_GPPUPPDN2 : constant := 16#00ec#;  --  ../bcm2835.h:813
   BCM2835_GPPUPPDN3 : constant := 16#00f0#;  --  ../bcm2835.h:814

   BCM2835_GPIO_PUD_ERROR : constant := 16#08#;  --  ../bcm2835.h:843

   BCM2835_PADS_GPIO_0_27 : constant := 16#002c#;  --  ../bcm2835.h:846
   BCM2835_PADS_GPIO_28_45 : constant := 16#0030#;  --  ../bcm2835.h:847
   BCM2835_PADS_GPIO_46_53 : constant := 16#0034#;  --  ../bcm2835.h:848
   --  unsupported macro: BCM2835_PAD_PASSWRD (0x5A << 24)

   BCM2835_PAD_SLEW_RATE_UNLIMITED : constant := 16#10#;  --  ../bcm2835.h:852
   BCM2835_PAD_HYSTERESIS_ENABLED : constant := 16#08#;  --  ../bcm2835.h:853
   BCM2835_PAD_DRIVE_2mA : constant := 16#00#;  --  ../bcm2835.h:854
   BCM2835_PAD_DRIVE_4mA : constant := 16#01#;  --  ../bcm2835.h:855
   BCM2835_PAD_DRIVE_6mA : constant := 16#02#;  --  ../bcm2835.h:856
   BCM2835_PAD_DRIVE_8mA : constant := 16#03#;  --  ../bcm2835.h:857
   BCM2835_PAD_DRIVE_10mA : constant := 16#04#;  --  ../bcm2835.h:858
   BCM2835_PAD_DRIVE_12mA : constant := 16#05#;  --  ../bcm2835.h:859
   BCM2835_PAD_DRIVE_14mA : constant := 16#06#;  --  ../bcm2835.h:860
   BCM2835_PAD_DRIVE_16mA : constant := 16#07#;  --  ../bcm2835.h:861

   BCM2835_AUX_IRQ : constant := 16#0000#;  --  ../bcm2835.h:973
   BCM2835_AUX_ENABLE : constant := 16#0004#;  --  ../bcm2835.h:974

   BCM2835_AUX_ENABLE_UART1 : constant := 16#01#;  --  ../bcm2835.h:976
   BCM2835_AUX_ENABLE_SPI0 : constant := 16#02#;  --  ../bcm2835.h:977
   BCM2835_AUX_ENABLE_SPI1 : constant := 16#04#;  --  ../bcm2835.h:978

   BCM2835_AUX_SPI_CNTL0 : constant := 16#0000#;  --  ../bcm2835.h:981
   BCM2835_AUX_SPI_CNTL1 : constant := 16#0004#;  --  ../bcm2835.h:982
   BCM2835_AUX_SPI_STAT : constant := 16#0008#;  --  ../bcm2835.h:983
   BCM2835_AUX_SPI_PEEK : constant := 16#000C#;  --  ../bcm2835.h:984
   BCM2835_AUX_SPI_IO : constant := 16#0020#;  --  ../bcm2835.h:985
   BCM2835_AUX_SPI_TXHOLD : constant := 16#0030#;  --  ../bcm2835.h:986

   BCM2835_AUX_SPI_CLOCK_MIN : constant := 30500;  --  ../bcm2835.h:988
   BCM2835_AUX_SPI_CLOCK_MAX : constant := 125000000;  --  ../bcm2835.h:989

   BCM2835_AUX_SPI_CNTL0_SPEED : constant := 16#FFF00000#;  --  ../bcm2835.h:991
   BCM2835_AUX_SPI_CNTL0_SPEED_MAX : constant := 16#FFF#;  --  ../bcm2835.h:992
   BCM2835_AUX_SPI_CNTL0_SPEED_SHIFT : constant := 20;  --  ../bcm2835.h:993

   BCM2835_AUX_SPI_CNTL0_CS0_N : constant := 16#000C0000#;  --  ../bcm2835.h:995
   BCM2835_AUX_SPI_CNTL0_CS1_N : constant := 16#000A0000#;  --  ../bcm2835.h:996
   BCM2835_AUX_SPI_CNTL0_CS2_N : constant := 16#00060000#;  --  ../bcm2835.h:997

   BCM2835_AUX_SPI_CNTL0_POSTINPUT : constant := 16#00010000#;  --  ../bcm2835.h:999
   BCM2835_AUX_SPI_CNTL0_VAR_CS : constant := 16#00008000#;  --  ../bcm2835.h:1000
   BCM2835_AUX_SPI_CNTL0_VAR_WIDTH : constant := 16#00004000#;  --  ../bcm2835.h:1001
   BCM2835_AUX_SPI_CNTL0_DOUTHOLD : constant := 16#00003000#;  --  ../bcm2835.h:1002
   BCM2835_AUX_SPI_CNTL0_ENABLE : constant := 16#00000800#;  --  ../bcm2835.h:1003
   BCM2835_AUX_SPI_CNTL0_CPHA_IN : constant := 16#00000400#;  --  ../bcm2835.h:1004
   BCM2835_AUX_SPI_CNTL0_CLEARFIFO : constant := 16#00000200#;  --  ../bcm2835.h:1005
   BCM2835_AUX_SPI_CNTL0_CPHA_OUT : constant := 16#00000100#;  --  ../bcm2835.h:1006
   BCM2835_AUX_SPI_CNTL0_CPOL : constant := 16#00000080#;  --  ../bcm2835.h:1007
   BCM2835_AUX_SPI_CNTL0_MSBF_OUT : constant := 16#00000040#;  --  ../bcm2835.h:1008
   BCM2835_AUX_SPI_CNTL0_SHIFTLEN : constant := 16#0000003F#;  --  ../bcm2835.h:1009

   BCM2835_AUX_SPI_CNTL1_CSHIGH : constant := 16#00000700#;  --  ../bcm2835.h:1011
   BCM2835_AUX_SPI_CNTL1_IDLE : constant := 16#00000080#;  --  ../bcm2835.h:1012
   BCM2835_AUX_SPI_CNTL1_TXEMPTY : constant := 16#00000040#;  --  ../bcm2835.h:1013
   BCM2835_AUX_SPI_CNTL1_MSBF_IN : constant := 16#00000002#;  --  ../bcm2835.h:1014
   BCM2835_AUX_SPI_CNTL1_KEEP_IN : constant := 16#00000001#;  --  ../bcm2835.h:1015

   BCM2835_AUX_SPI_STAT_TX_LVL : constant := 16#F0000000#;  --  ../bcm2835.h:1017
   BCM2835_AUX_SPI_STAT_RX_LVL : constant := 16#00F00000#;  --  ../bcm2835.h:1018
   BCM2835_AUX_SPI_STAT_TX_FULL : constant := 16#00000400#;  --  ../bcm2835.h:1019
   BCM2835_AUX_SPI_STAT_TX_EMPTY : constant := 16#00000200#;  --  ../bcm2835.h:1020
   BCM2835_AUX_SPI_STAT_RX_FULL : constant := 16#00000100#;  --  ../bcm2835.h:1021
   BCM2835_AUX_SPI_STAT_RX_EMPTY : constant := 16#00000080#;  --  ../bcm2835.h:1022
   BCM2835_AUX_SPI_STAT_BUSY : constant := 16#00000040#;  --  ../bcm2835.h:1023
   BCM2835_AUX_SPI_STAT_BITCOUNT : constant := 16#0000003F#;  --  ../bcm2835.h:1024

   BCM2835_SPI0_CS : constant := 16#0000#;  --  ../bcm2835.h:1030
   BCM2835_SPI0_FIFO : constant := 16#0004#;  --  ../bcm2835.h:1031
   BCM2835_SPI0_CLK : constant := 16#0008#;  --  ../bcm2835.h:1032
   BCM2835_SPI0_DLEN : constant := 16#000c#;  --  ../bcm2835.h:1033
   BCM2835_SPI0_LTOH : constant := 16#0010#;  --  ../bcm2835.h:1034
   BCM2835_SPI0_DC : constant := 16#0014#;  --  ../bcm2835.h:1035

   BCM2835_SPI0_CS_LEN_LONG : constant := 16#02000000#;  --  ../bcm2835.h:1038
   BCM2835_SPI0_CS_DMA_LEN : constant := 16#01000000#;  --  ../bcm2835.h:1039
   BCM2835_SPI0_CS_CSPOL2 : constant := 16#00800000#;  --  ../bcm2835.h:1040
   BCM2835_SPI0_CS_CSPOL1 : constant := 16#00400000#;  --  ../bcm2835.h:1041
   BCM2835_SPI0_CS_CSPOL0 : constant := 16#00200000#;  --  ../bcm2835.h:1042
   BCM2835_SPI0_CS_RXF : constant := 16#00100000#;  --  ../bcm2835.h:1043
   BCM2835_SPI0_CS_RXR : constant := 16#00080000#;  --  ../bcm2835.h:1044
   BCM2835_SPI0_CS_TXD : constant := 16#00040000#;  --  ../bcm2835.h:1045
   BCM2835_SPI0_CS_RXD : constant := 16#00020000#;  --  ../bcm2835.h:1046
   BCM2835_SPI0_CS_DONE : constant := 16#00010000#;  --  ../bcm2835.h:1047
   BCM2835_SPI0_CS_TE_EN : constant := 16#00008000#;  --  ../bcm2835.h:1048
   BCM2835_SPI0_CS_LMONO : constant := 16#00004000#;  --  ../bcm2835.h:1049
   BCM2835_SPI0_CS_LEN : constant := 16#00002000#;  --  ../bcm2835.h:1050
   BCM2835_SPI0_CS_REN : constant := 16#00001000#;  --  ../bcm2835.h:1051
   BCM2835_SPI0_CS_ADCS : constant := 16#00000800#;  --  ../bcm2835.h:1052
   BCM2835_SPI0_CS_INTR : constant := 16#00000400#;  --  ../bcm2835.h:1053
   BCM2835_SPI0_CS_INTD : constant := 16#00000200#;  --  ../bcm2835.h:1054
   BCM2835_SPI0_CS_DMAEN : constant := 16#00000100#;  --  ../bcm2835.h:1055
   BCM2835_SPI0_CS_TA : constant := 16#00000080#;  --  ../bcm2835.h:1056
   BCM2835_SPI0_CS_CSPOL : constant := 16#00000040#;  --  ../bcm2835.h:1057
   BCM2835_SPI0_CS_CLEAR : constant := 16#00000030#;  --  ../bcm2835.h:1058
   BCM2835_SPI0_CS_CLEAR_RX : constant := 16#00000020#;  --  ../bcm2835.h:1059
   BCM2835_SPI0_CS_CLEAR_TX : constant := 16#00000010#;  --  ../bcm2835.h:1060
   BCM2835_SPI0_CS_CPOL : constant := 16#00000008#;  --  ../bcm2835.h:1061
   BCM2835_SPI0_CS_CPHA : constant := 16#00000004#;  --  ../bcm2835.h:1062
   BCM2835_SPI0_CS_CS : constant := 16#00000003#;  --  ../bcm2835.h:1063

   BCM2835_BSC_C : constant := 16#0000#;  --  ../bcm2835.h:1135
   BCM2835_BSC_S : constant := 16#0004#;  --  ../bcm2835.h:1136
   BCM2835_BSC_DLEN : constant := 16#0008#;  --  ../bcm2835.h:1137
   BCM2835_BSC_A : constant := 16#000c#;  --  ../bcm2835.h:1138
   BCM2835_BSC_FIFO : constant := 16#0010#;  --  ../bcm2835.h:1139
   BCM2835_BSC_DIV : constant := 16#0014#;  --  ../bcm2835.h:1140
   BCM2835_BSC_DEL : constant := 16#0018#;  --  ../bcm2835.h:1141
   BCM2835_BSC_CLKT : constant := 16#001c#;  --  ../bcm2835.h:1142

   BCM2835_BSC_C_I2CEN : constant := 16#00008000#;  --  ../bcm2835.h:1145
   BCM2835_BSC_C_INTR : constant := 16#00000400#;  --  ../bcm2835.h:1146
   BCM2835_BSC_C_INTT : constant := 16#00000200#;  --  ../bcm2835.h:1147
   BCM2835_BSC_C_INTD : constant := 16#00000100#;  --  ../bcm2835.h:1148
   BCM2835_BSC_C_ST : constant := 16#00000080#;  --  ../bcm2835.h:1149
   BCM2835_BSC_C_CLEAR_1 : constant := 16#00000020#;  --  ../bcm2835.h:1150
   BCM2835_BSC_C_CLEAR_2 : constant := 16#00000010#;  --  ../bcm2835.h:1151
   BCM2835_BSC_C_READ : constant := 16#00000001#;  --  ../bcm2835.h:1152

   BCM2835_BSC_S_CLKT : constant := 16#00000200#;  --  ../bcm2835.h:1155
   BCM2835_BSC_S_ERR : constant := 16#00000100#;  --  ../bcm2835.h:1156
   BCM2835_BSC_S_RXF : constant := 16#00000080#;  --  ../bcm2835.h:1157
   BCM2835_BSC_S_TXE : constant := 16#00000040#;  --  ../bcm2835.h:1158
   BCM2835_BSC_S_RXD : constant := 16#00000020#;  --  ../bcm2835.h:1159
   BCM2835_BSC_S_TXD : constant := 16#00000010#;  --  ../bcm2835.h:1160
   BCM2835_BSC_S_RXR : constant := 16#00000008#;  --  ../bcm2835.h:1161
   BCM2835_BSC_S_TXW : constant := 16#00000004#;  --  ../bcm2835.h:1162
   BCM2835_BSC_S_DONE : constant := 16#00000002#;  --  ../bcm2835.h:1163
   BCM2835_BSC_S_TA : constant := 16#00000001#;  --  ../bcm2835.h:1164

   BCM2835_BSC_FIFO_SIZE : constant := 16;  --  ../bcm2835.h:1166

   BCM2835_ST_CS : constant := 16#0000#;  --  ../bcm2835.h:1202
   BCM2835_ST_CLO : constant := 16#0004#;  --  ../bcm2835.h:1203
   BCM2835_ST_CHI : constant := 16#0008#;  --  ../bcm2835.h:1204

   BCM2835_PWM_CONTROL : constant := 0;  --  ../bcm2835.h:1210
   BCM2835_PWM_STATUS : constant := 1;  --  ../bcm2835.h:1211
   BCM2835_PWM_DMAC : constant := 2;  --  ../bcm2835.h:1212
   BCM2835_PWM0_RANGE : constant := 4;  --  ../bcm2835.h:1213
   BCM2835_PWM0_DATA : constant := 5;  --  ../bcm2835.h:1214
   BCM2835_PWM_FIF1 : constant := 6;  --  ../bcm2835.h:1215
   BCM2835_PWM1_RANGE : constant := 8;  --  ../bcm2835.h:1216
   BCM2835_PWM1_DATA : constant := 9;  --  ../bcm2835.h:1217

   BCM2835_PWMCLK_CNTL : constant := 40;  --  ../bcm2835.h:1220
   BCM2835_PWMCLK_DIV : constant := 41;  --  ../bcm2835.h:1221
   --  unsupported macro: BCM2835_PWM_PASSWRD (0x5A << 24)

   BCM2835_PWM1_MS_MODE : constant := 16#8000#;  --  ../bcm2835.h:1224
   BCM2835_PWM1_USEFIFO : constant := 16#2000#;  --  ../bcm2835.h:1225
   BCM2835_PWM1_REVPOLAR : constant := 16#1000#;  --  ../bcm2835.h:1226
   BCM2835_PWM1_OFFSTATE : constant := 16#0800#;  --  ../bcm2835.h:1227
   BCM2835_PWM1_REPEATFF : constant := 16#0400#;  --  ../bcm2835.h:1228
   BCM2835_PWM1_SERIAL : constant := 16#0200#;  --  ../bcm2835.h:1229
   BCM2835_PWM1_ENABLE : constant := 16#0100#;  --  ../bcm2835.h:1230

   BCM2835_PWM0_MS_MODE : constant := 16#0080#;  --  ../bcm2835.h:1232
   BCM2835_PWM_CLEAR_FIFO : constant := 16#0040#;  --  ../bcm2835.h:1233
   BCM2835_PWM0_USEFIFO : constant := 16#0020#;  --  ../bcm2835.h:1234
   BCM2835_PWM0_REVPOLAR : constant := 16#0010#;  --  ../bcm2835.h:1235
   BCM2835_PWM0_OFFSTATE : constant := 16#0008#;  --  ../bcm2835.h:1236
   BCM2835_PWM0_REPEATFF : constant := 16#0004#;  --  ../bcm2835.h:1237
   BCM2835_PWM0_SERIAL : constant := 16#0002#;  --  ../bcm2835.h:1238
   BCM2835_PWM0_ENABLE : constant := 16#0001#;  --  ../bcm2835.h:1239
   --  arg-macro: procedure delay (x)
   --    bcm2835_delay(x)
   --  arg-macro: procedure delayMicroseconds (x)
   --    bcm2835_delayMicroseconds(x)

   bcm2835_peripherals_base : aliased aarch64_linux_gnu_sys_types_h.off_t  -- ../bcm2835.h:692
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_peripherals_base";

   bcm2835_peripherals_size : aliased stddef_h.size_t  -- ../bcm2835.h:694
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_peripherals_size";

   bcm2835_peripherals : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:697
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_peripherals";

   bcm2835_st : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:702
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_st";

   bcm2835_gpio : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:707
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio";

   bcm2835_pwm : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:712
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_pwm";

   bcm2835_clk : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:717
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_clk";

   bcm2835_pads : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:722
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_pads";

   bcm2835_spi0 : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:727
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi0";

   bcm2835_bsc0 : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:732
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_bsc0";

   bcm2835_bsc1 : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:737
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_bsc1";

   bcm2835_aux : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:742
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux";

   bcm2835_spi1 : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:747
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi1";

   subtype bcm2835RegisterBase is unsigned;
   bcm2835RegisterBase_BCM2835_REGBASE_ST : constant bcm2835RegisterBase := 1;
   bcm2835RegisterBase_BCM2835_REGBASE_GPIO : constant bcm2835RegisterBase := 2;
   bcm2835RegisterBase_BCM2835_REGBASE_PWM : constant bcm2835RegisterBase := 3;
   bcm2835RegisterBase_BCM2835_REGBASE_CLK : constant bcm2835RegisterBase := 4;
   bcm2835RegisterBase_BCM2835_REGBASE_PADS : constant bcm2835RegisterBase := 5;
   bcm2835RegisterBase_BCM2835_REGBASE_SPI0 : constant bcm2835RegisterBase := 6;
   bcm2835RegisterBase_BCM2835_REGBASE_BSC0 : constant bcm2835RegisterBase := 7;
   bcm2835RegisterBase_BCM2835_REGBASE_BSC1 : constant bcm2835RegisterBase := 8;
   bcm2835RegisterBase_BCM2835_REGBASE_AUX : constant bcm2835RegisterBase := 9;
   bcm2835RegisterBase_BCM2835_REGBASE_SPI1 : constant bcm2835RegisterBase := 10;  -- ../bcm2835.h:765

   subtype bcm2835FunctionSelect is unsigned;
   bcm2835FunctionSelect_BCM2835_GPIO_FSEL_INPT : constant bcm2835FunctionSelect := 0;
   bcm2835FunctionSelect_BCM2835_GPIO_FSEL_OUTP : constant bcm2835FunctionSelect := 1;
   bcm2835FunctionSelect_BCM2835_GPIO_FSEL_ALT0 : constant bcm2835FunctionSelect := 4;
   bcm2835FunctionSelect_BCM2835_GPIO_FSEL_ALT1 : constant bcm2835FunctionSelect := 5;
   bcm2835FunctionSelect_BCM2835_GPIO_FSEL_ALT2 : constant bcm2835FunctionSelect := 6;
   bcm2835FunctionSelect_BCM2835_GPIO_FSEL_ALT3 : constant bcm2835FunctionSelect := 7;
   bcm2835FunctionSelect_BCM2835_GPIO_FSEL_ALT4 : constant bcm2835FunctionSelect := 3;
   bcm2835FunctionSelect_BCM2835_GPIO_FSEL_ALT5 : constant bcm2835FunctionSelect := 2;
   bcm2835FunctionSelect_BCM2835_GPIO_FSEL_MASK : constant bcm2835FunctionSelect := 7;  -- ../bcm2835.h:830

   type bcm2835PUDControl is 
     (BCM2835_GPIO_PUD_OFF,
      BCM2835_GPIO_PUD_DOWN,
      BCM2835_GPIO_PUD_UP)
   with Convention => C;  -- ../bcm2835.h:840

   type bcm2835PadGroup is 
     (BCM2835_PAD_GROUP_GPIO_0_27,
      BCM2835_PAD_GROUP_GPIO_28_45,
      BCM2835_PAD_GROUP_GPIO_46_53)
   with Convention => C;  -- ../bcm2835.h:871

   subtype RPiGPIOPin is unsigned;
   RPiGPIOPin_RPI_GPIO_P1_03 : constant RPiGPIOPin := 0;
   RPiGPIOPin_RPI_GPIO_P1_05 : constant RPiGPIOPin := 1;
   RPiGPIOPin_RPI_GPIO_P1_07 : constant RPiGPIOPin := 4;
   RPiGPIOPin_RPI_GPIO_P1_08 : constant RPiGPIOPin := 14;
   RPiGPIOPin_RPI_GPIO_P1_10 : constant RPiGPIOPin := 15;
   RPiGPIOPin_RPI_GPIO_P1_11 : constant RPiGPIOPin := 17;
   RPiGPIOPin_RPI_GPIO_P1_12 : constant RPiGPIOPin := 18;
   RPiGPIOPin_RPI_GPIO_P1_13 : constant RPiGPIOPin := 21;
   RPiGPIOPin_RPI_GPIO_P1_15 : constant RPiGPIOPin := 22;
   RPiGPIOPin_RPI_GPIO_P1_16 : constant RPiGPIOPin := 23;
   RPiGPIOPin_RPI_GPIO_P1_18 : constant RPiGPIOPin := 24;
   RPiGPIOPin_RPI_GPIO_P1_19 : constant RPiGPIOPin := 10;
   RPiGPIOPin_RPI_GPIO_P1_21 : constant RPiGPIOPin := 9;
   RPiGPIOPin_RPI_GPIO_P1_22 : constant RPiGPIOPin := 25;
   RPiGPIOPin_RPI_GPIO_P1_23 : constant RPiGPIOPin := 11;
   RPiGPIOPin_RPI_GPIO_P1_24 : constant RPiGPIOPin := 8;
   RPiGPIOPin_RPI_GPIO_P1_26 : constant RPiGPIOPin := 7;
   RPiGPIOPin_RPI_V2_GPIO_P1_03 : constant RPiGPIOPin := 2;
   RPiGPIOPin_RPI_V2_GPIO_P1_05 : constant RPiGPIOPin := 3;
   RPiGPIOPin_RPI_V2_GPIO_P1_07 : constant RPiGPIOPin := 4;
   RPiGPIOPin_RPI_V2_GPIO_P1_08 : constant RPiGPIOPin := 14;
   RPiGPIOPin_RPI_V2_GPIO_P1_10 : constant RPiGPIOPin := 15;
   RPiGPIOPin_RPI_V2_GPIO_P1_11 : constant RPiGPIOPin := 17;
   RPiGPIOPin_RPI_V2_GPIO_P1_12 : constant RPiGPIOPin := 18;
   RPiGPIOPin_RPI_V2_GPIO_P1_13 : constant RPiGPIOPin := 27;
   RPiGPIOPin_RPI_V2_GPIO_P1_15 : constant RPiGPIOPin := 22;
   RPiGPIOPin_RPI_V2_GPIO_P1_16 : constant RPiGPIOPin := 23;
   RPiGPIOPin_RPI_V2_GPIO_P1_18 : constant RPiGPIOPin := 24;
   RPiGPIOPin_RPI_V2_GPIO_P1_19 : constant RPiGPIOPin := 10;
   RPiGPIOPin_RPI_V2_GPIO_P1_21 : constant RPiGPIOPin := 9;
   RPiGPIOPin_RPI_V2_GPIO_P1_22 : constant RPiGPIOPin := 25;
   RPiGPIOPin_RPI_V2_GPIO_P1_23 : constant RPiGPIOPin := 11;
   RPiGPIOPin_RPI_V2_GPIO_P1_24 : constant RPiGPIOPin := 8;
   RPiGPIOPin_RPI_V2_GPIO_P1_26 : constant RPiGPIOPin := 7;
   RPiGPIOPin_RPI_V2_GPIO_P1_29 : constant RPiGPIOPin := 5;
   RPiGPIOPin_RPI_V2_GPIO_P1_31 : constant RPiGPIOPin := 6;
   RPiGPIOPin_RPI_V2_GPIO_P1_32 : constant RPiGPIOPin := 12;
   RPiGPIOPin_RPI_V2_GPIO_P1_33 : constant RPiGPIOPin := 13;
   RPiGPIOPin_RPI_V2_GPIO_P1_35 : constant RPiGPIOPin := 19;
   RPiGPIOPin_RPI_V2_GPIO_P1_36 : constant RPiGPIOPin := 16;
   RPiGPIOPin_RPI_V2_GPIO_P1_37 : constant RPiGPIOPin := 26;
   RPiGPIOPin_RPI_V2_GPIO_P1_38 : constant RPiGPIOPin := 20;
   RPiGPIOPin_RPI_V2_GPIO_P1_40 : constant RPiGPIOPin := 21;
   RPiGPIOPin_RPI_V2_GPIO_P5_03 : constant RPiGPIOPin := 28;
   RPiGPIOPin_RPI_V2_GPIO_P5_04 : constant RPiGPIOPin := 29;
   RPiGPIOPin_RPI_V2_GPIO_P5_05 : constant RPiGPIOPin := 30;
   RPiGPIOPin_RPI_V2_GPIO_P5_06 : constant RPiGPIOPin := 31;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_03 : constant RPiGPIOPin := 2;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_05 : constant RPiGPIOPin := 3;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_07 : constant RPiGPIOPin := 4;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_08 : constant RPiGPIOPin := 14;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_10 : constant RPiGPIOPin := 15;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_11 : constant RPiGPIOPin := 17;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_12 : constant RPiGPIOPin := 18;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_13 : constant RPiGPIOPin := 27;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_15 : constant RPiGPIOPin := 22;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_16 : constant RPiGPIOPin := 23;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_18 : constant RPiGPIOPin := 24;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_19 : constant RPiGPIOPin := 10;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_21 : constant RPiGPIOPin := 9;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_22 : constant RPiGPIOPin := 25;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_23 : constant RPiGPIOPin := 11;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_24 : constant RPiGPIOPin := 8;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_26 : constant RPiGPIOPin := 7;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_29 : constant RPiGPIOPin := 5;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_31 : constant RPiGPIOPin := 6;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_32 : constant RPiGPIOPin := 12;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_33 : constant RPiGPIOPin := 13;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_35 : constant RPiGPIOPin := 19;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_36 : constant RPiGPIOPin := 16;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_37 : constant RPiGPIOPin := 26;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_38 : constant RPiGPIOPin := 20;
   RPiGPIOPin_RPI_BPLUS_GPIO_J8_40 : constant RPiGPIOPin := 21;  -- ../bcm2835.h:968

   type bcm2835SPIBitOrder is 
     (BCM2835_SPI_BIT_ORDER_LSBFIRST,
      BCM2835_SPI_BIT_ORDER_MSBFIRST)
   with Convention => C;  -- ../bcm2835.h:1072

   type bcm2835SPIMode is 
     (BCM2835_SPI_MODE0,
      BCM2835_SPI_MODE1,
      BCM2835_SPI_MODE2,
      BCM2835_SPI_MODE3)
   with Convention => C;  -- ../bcm2835.h:1083

   type bcm2835SPIChipSelect is 
     (BCM2835_SPI_CS0,
      BCM2835_SPI_CS1,
      BCM2835_SPI_CS2,
      BCM2835_SPI_CS_NONE)
   with Convention => C;  -- ../bcm2835.h:1094

   subtype bcm2835SPIClockDivider is unsigned;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_65536 : constant bcm2835SPIClockDivider := 0;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_32768 : constant bcm2835SPIClockDivider := 32768;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_16384 : constant bcm2835SPIClockDivider := 16384;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_8192 : constant bcm2835SPIClockDivider := 8192;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_4096 : constant bcm2835SPIClockDivider := 4096;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_2048 : constant bcm2835SPIClockDivider := 2048;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_1024 : constant bcm2835SPIClockDivider := 1024;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_512 : constant bcm2835SPIClockDivider := 512;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_256 : constant bcm2835SPIClockDivider := 256;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_128 : constant bcm2835SPIClockDivider := 128;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_64 : constant bcm2835SPIClockDivider := 64;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_32 : constant bcm2835SPIClockDivider := 32;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_16 : constant bcm2835SPIClockDivider := 16;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_8 : constant bcm2835SPIClockDivider := 8;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_4 : constant bcm2835SPIClockDivider := 4;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_2 : constant bcm2835SPIClockDivider := 2;
   bcm2835SPIClockDivider_BCM2835_SPI_CLOCK_DIVIDER_1 : constant bcm2835SPIClockDivider := 1;  -- ../bcm2835.h:1129

   subtype bcm2835I2CClockDivider is unsigned;
   bcm2835I2CClockDivider_BCM2835_I2C_CLOCK_DIVIDER_2500 : constant bcm2835I2CClockDivider := 2500;
   bcm2835I2CClockDivider_BCM2835_I2C_CLOCK_DIVIDER_626 : constant bcm2835I2CClockDivider := 626;
   bcm2835I2CClockDivider_BCM2835_I2C_CLOCK_DIVIDER_150 : constant bcm2835I2CClockDivider := 150;
   bcm2835I2CClockDivider_BCM2835_I2C_CLOCK_DIVIDER_148 : constant bcm2835I2CClockDivider := 148;  -- ../bcm2835.h:1178

   subtype bcm2835I2CReasonCodes is unsigned;
   bcm2835I2CReasonCodes_BCM2835_I2C_REASON_OK : constant bcm2835I2CReasonCodes := 0;
   bcm2835I2CReasonCodes_BCM2835_I2C_REASON_ERROR_NACK : constant bcm2835I2CReasonCodes := 1;
   bcm2835I2CReasonCodes_BCM2835_I2C_REASON_ERROR_CLKT : constant bcm2835I2CReasonCodes := 2;
   bcm2835I2CReasonCodes_BCM2835_I2C_REASON_ERROR_DATA : constant bcm2835I2CReasonCodes := 4;  -- ../bcm2835.h:1189

   subtype bcm2835PWMClockDivider is unsigned;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_2048 : constant bcm2835PWMClockDivider := 2048;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_1024 : constant bcm2835PWMClockDivider := 1024;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_512 : constant bcm2835PWMClockDivider := 512;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_256 : constant bcm2835PWMClockDivider := 256;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_128 : constant bcm2835PWMClockDivider := 128;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_64 : constant bcm2835PWMClockDivider := 64;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_32 : constant bcm2835PWMClockDivider := 32;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_16 : constant bcm2835PWMClockDivider := 16;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_8 : constant bcm2835PWMClockDivider := 8;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_4 : constant bcm2835PWMClockDivider := 4;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_2 : constant bcm2835PWMClockDivider := 2;
   bcm2835PWMClockDivider_BCM2835_PWM_CLOCK_DIVIDER_1 : constant bcm2835PWMClockDivider := 1;  -- ../bcm2835.h:1261

   function bcm2835_init return int  -- ../bcm2835.h:1291
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_init";

   function bcm2835_close return int  -- ../bcm2835.h:1296
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_close";

   procedure bcm2835_set_debug (debug : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1305
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_set_debug";

   function bcm2835_version return unsigned  -- ../bcm2835.h:1310
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_version";

   function bcm2835_regbase (regbase : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t) return access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:1327
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_regbase";

   function bcm2835_peri_read (paddr : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:1337
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_peri_read";

   function bcm2835_peri_read_nb (paddr : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:1349
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_peri_read_nb";

   procedure bcm2835_peri_write (paddr : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t; value : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1363
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_peri_write";

   procedure bcm2835_peri_write_nb (paddr : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t; value : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1377
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_peri_write_nb";

   procedure bcm2835_peri_set_bits
     (paddr : access aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t;
      value : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t;
      mask : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1391
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_peri_set_bits";

   procedure bcm2835_gpio_fsel (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t; mode : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1405
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_fsel";

   procedure bcm2835_gpio_set (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1412
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_set";

   procedure bcm2835_gpio_clr (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1419
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_clr";

   procedure bcm2835_gpio_set_multi (mask : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1426
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_set_multi";

   procedure bcm2835_gpio_clr_multi (mask : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1433
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_clr_multi";

   function bcm2835_gpio_lev (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t  -- ../bcm2835.h:1441
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_lev";

   function bcm2835_gpio_eds (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t  -- ../bcm2835.h:1451
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_eds";

   function bcm2835_gpio_eds_multi (mask : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:1458
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_eds_multi";

   procedure bcm2835_gpio_set_eds (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1465
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_set_eds";

   procedure bcm2835_gpio_set_eds_multi (mask : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1471
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_set_eds_multi";

   procedure bcm2835_gpio_ren (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1481
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_ren";

   procedure bcm2835_gpio_clr_ren (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1486
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_clr_ren";

   procedure bcm2835_gpio_fen (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1496
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_fen";

   procedure bcm2835_gpio_clr_fen (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1501
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_clr_fen";

   procedure bcm2835_gpio_hen (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1507
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_hen";

   procedure bcm2835_gpio_clr_hen (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1512
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_clr_hen";

   procedure bcm2835_gpio_len (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1518
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_len";

   procedure bcm2835_gpio_clr_len (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1523
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_clr_len";

   procedure bcm2835_gpio_aren (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1531
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_aren";

   procedure bcm2835_gpio_clr_aren (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1536
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_clr_aren";

   procedure bcm2835_gpio_afen (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1544
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_afen";

   procedure bcm2835_gpio_clr_afen (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1549
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_clr_afen";

   procedure bcm2835_gpio_pud (pud : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1559
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_pud";

   procedure bcm2835_gpio_pudclk (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t; on : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1571
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_pudclk";

   function bcm2835_gpio_pad (group : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t  -- ../bcm2835.h:1578
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_pad";

   procedure bcm2835_gpio_set_pad (group : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t; control : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1587
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_set_pad";

   procedure bcm2835_delay (millis : unsigned)  -- ../bcm2835.h:1599
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_delay";

   procedure bcm2835_delayMicroseconds (micros : aarch64_linux_gnu_bits_stdint_uintn_h.uint64_t)  -- ../bcm2835.h:1614
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_delayMicroseconds";

   procedure bcm2835_gpio_write (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t; on : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1620
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_write";

   procedure bcm2835_gpio_write_multi (mask : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t; on : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1626
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_write_multi";

   procedure bcm2835_gpio_write_mask (value : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t; mask : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1632
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_write_mask";

   procedure bcm2835_gpio_set_pud (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t; pud : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1639
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_set_pud";

   function bcm2835_gpio_get_pud (pin : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t  -- ../bcm2835.h:1647
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_gpio_get_pud";

   function bcm2835_spi_begin return int  -- ../bcm2835.h:1665
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_begin";

   procedure bcm2835_spi_end  -- ../bcm2835.h:1671
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_end";

   procedure bcm2835_spi_setBitOrder (order : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1680
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_setBitOrder";

   procedure bcm2835_spi_setClockDivider (divider : aarch64_linux_gnu_bits_stdint_uintn_h.uint16_t)  -- ../bcm2835.h:1687
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_setClockDivider";

   procedure bcm2835_spi_set_speed_hz (speed_hz : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1693
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_set_speed_hz";

   procedure bcm2835_spi_setDataMode (mode : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1700
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_setDataMode";

   procedure bcm2835_spi_chipSelect (cs : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1708
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_chipSelect";

   procedure bcm2835_spi_setChipSelectPolarity (cs : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t; active : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1718
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_setChipSelectPolarity";

   function bcm2835_spi_transfer (value : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t  -- ../bcm2835.h:1730
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_transfer";

   procedure bcm2835_spi_transfernb
     (tbuf : Interfaces.C.Strings.chars_ptr;
      rbuf : Interfaces.C.Strings.chars_ptr;
      len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1743
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_transfernb";

   procedure bcm2835_spi_transfern (buf : Interfaces.C.Strings.chars_ptr; len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1752
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_transfern";

   procedure bcm2835_spi_writenb (buf : Interfaces.C.Strings.chars_ptr; len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1760
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_writenb";

   procedure bcm2835_spi_write (data : aarch64_linux_gnu_bits_stdint_uintn_h.uint16_t)  -- ../bcm2835.h:1770
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_spi_write";

   function bcm2835_aux_spi_begin return int  -- ../bcm2835.h:1777
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux_spi_begin";

   procedure bcm2835_aux_spi_end  -- ../bcm2835.h:1783
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux_spi_end";

   procedure bcm2835_aux_spi_setClockDivider (divider : aarch64_linux_gnu_bits_stdint_uintn_h.uint16_t)  -- ../bcm2835.h:1788
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux_spi_setClockDivider";

   function bcm2835_aux_spi_CalcClockDivider (speed_hz : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint16_t  -- ../bcm2835.h:1795
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux_spi_CalcClockDivider";

   procedure bcm2835_aux_spi_write (data : aarch64_linux_gnu_bits_stdint_uintn_h.uint16_t)  -- ../bcm2835.h:1803
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux_spi_write";

   procedure bcm2835_aux_spi_writenb (buf : Interfaces.C.Strings.chars_ptr; len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1810
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux_spi_writenb";

   procedure bcm2835_aux_spi_transfern (buf : Interfaces.C.Strings.chars_ptr; len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1819
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux_spi_transfern";

   procedure bcm2835_aux_spi_transfernb
     (tbuf : Interfaces.C.Strings.chars_ptr;
      rbuf : Interfaces.C.Strings.chars_ptr;
      len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1829
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux_spi_transfernb";

   function bcm2835_aux_spi_transfer (value : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t  -- ../bcm2835.h:1838
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_aux_spi_transfer";

   function bcm2835_i2c_begin return int  -- ../bcm2835.h:1856
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_i2c_begin";

   procedure bcm2835_i2c_end  -- ../bcm2835.h:1862
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_i2c_end";

   procedure bcm2835_i2c_setSlaveAddress (addr : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1867
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_i2c_setSlaveAddress";

   procedure bcm2835_i2c_setClockDivider (divider : aarch64_linux_gnu_bits_stdint_uintn_h.uint16_t)  -- ../bcm2835.h:1873
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_i2c_setClockDivider";

   procedure bcm2835_i2c_set_baudrate (baudrate : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1881
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_i2c_set_baudrate";

   function bcm2835_i2c_write (buf : Interfaces.C.Strings.chars_ptr; len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t  -- ../bcm2835.h:1889
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_i2c_write";

   function bcm2835_i2c_read (buf : Interfaces.C.Strings.chars_ptr; len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t  -- ../bcm2835.h:1897
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_i2c_read";

   function bcm2835_i2c_read_register_rs
     (regaddr : Interfaces.C.Strings.chars_ptr;
      buf : Interfaces.C.Strings.chars_ptr;
      len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t  -- ../bcm2835.h:1913
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_i2c_read_register_rs";

   function bcm2835_i2c_write_read_rs
     (cmds : Interfaces.C.Strings.chars_ptr;
      cmds_len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t;
      buf : Interfaces.C.Strings.chars_ptr;
      buf_len : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t) return aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t  -- ../bcm2835.h:1925
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_i2c_write_read_rs";

   function bcm2835_st_read return aarch64_linux_gnu_bits_stdint_uintn_h.uint64_t  -- ../bcm2835.h:1937
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_st_read";

   procedure bcm2835_st_delay (offset_micros : aarch64_linux_gnu_bits_stdint_uintn_h.uint64_t; micros : aarch64_linux_gnu_bits_stdint_uintn_h.uint64_t)  -- ../bcm2835.h:1943
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_st_delay";

   procedure bcm2835_pwm_set_clock (divisor : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1960
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_pwm_set_clock";

   procedure bcm2835_pwm_set_mode
     (channel : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t;
      markspace : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t;
      enabled : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t)  -- ../bcm2835.h:1968
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_pwm_set_mode";

   procedure bcm2835_pwm_set_range (channel : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t; c_range : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1975
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_pwm_set_range";

   procedure bcm2835_pwm_set_data (channel : aarch64_linux_gnu_bits_stdint_uintn_h.uint8_t; data : aarch64_linux_gnu_bits_stdint_uintn_h.uint32_t)  -- ../bcm2835.h:1983
   with Import => True, 
        Convention => C, 
        External_Name => "bcm2835_pwm_set_data";

end bcm2835_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
