pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package aarch64_linux_gnu_bits_byteswap_h is

   --  skipped func __bswap_16

   --  skipped func __bswap_32

   --  skipped func __bswap_64

end aarch64_linux_gnu_bits_byteswap_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
