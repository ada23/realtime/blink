pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;
with System;

package aarch64_linux_gnu_cpp_12_bits_cppconfig_h is

   subtype size_t is unsigned_long;  -- /usr/include/aarch64-linux-gnu/c++/12/bits/c++config.h:298

   subtype ptrdiff_t is long;  -- /usr/include/aarch64-linux-gnu/c++/12/bits/c++config.h:299

   subtype nullptr_t is System.Address;  -- /usr/include/aarch64-linux-gnu/c++/12/bits/c++config.h:302

   --  skipped func __terminate

   procedure c_terminate  -- /usr/include/aarch64-linux-gnu/c++/12/bits/c++config.h:311
   with Import => True, 
        Convention => CPP, 
        External_Name => "_ZSt9terminatev";

   --  skipped func __is_constant_evaluated

end aarch64_linux_gnu_cpp_12_bits_cppconfig_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
