pragma Ada_2012;

pragma Style_Checks (Off);
pragma Warnings (Off, "-gnatwu");

with Interfaces.C; use Interfaces.C;

package stddef_h is

   --  unsupported macro: NULL __null
   subtype size_t is unsigned_long;  -- /usr/lib/gcc/aarch64-linux-gnu/12/include/stddef.h:214

end stddef_h;

pragma Style_Checks (On);
pragma Warnings (On, "-gnatwu");
