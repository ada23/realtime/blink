with Ada.Text_IO;
with Raspio.GPIO;

procedure blink is
   use Raspio.GPIO;
begin
   Raspio.Initialize;
   declare
      LED : constant Raspio.GPIO.Pin_Type :=
        Raspio.GPIO.Create
          (Pin_ID            => GPIO_P1_07, 
          Mode => Output,
          Internal_Resistor => Pull_Down);
   begin
      loop
         Raspio.GPIO.Turn_On(LED) ; 
         delay 2.0 ;
         Raspio.GPIO.Turn_Off( LED );
         delay 2.0 ;
      end loop;
   end;
end blink;